using UnityEngine;

namespace Code
{
	public class Border : MonoBehaviour
	{
		private void OnTriggerEnter2D(Collider2D other)
		{
			if (other.TryGetComponent(out PopSystem ball))
				ball.PopBall(true);
		}
	}
}
