using UnityEngine;

namespace Code
{
	public class BallSpawner : MonoBehaviour
	{
		[Header("Dependencies")]
		[SerializeField] private ScoreController _scoreController;
		[SerializeField] private Transform _ballContainer;

		[Header("Data")]
		[SerializeField] private Ball _ballPrefab;

		[Header("Setting")]
		[Range(0.2f, 1f)]
		[SerializeField] private float _timeBetweenSpawns;
		[Range(0.1f, 0.5f)]
		[SerializeField] private float _minSize;
		[Range(0.5f, 1.5f)]
		[SerializeField] private float _maxSize;
		[SerializeField] private Transform _rightBorderPosition;
		[SerializeField] private Transform _leftBorderPosition;

		private float spawnTimer;
		
		private void Update()
		{
			if (spawnTimer <= 0)
            {
				SpawnBall();
				spawnTimer = _timeBetweenSpawns;
			}
			
			spawnTimer -= Time.deltaTime;
		}
		
		private void SpawnBall()
		{
			GameObject ball = Instantiate(_ballPrefab.gameObject, GetRandomPosition(), Quaternion.identity, _ballContainer);

			float randomSize = Random.Range(_minSize, _maxSize);
			ball.GetComponent<Transform>().localScale = new Vector3(randomSize, randomSize, 0);

			ball.TryGetComponent<SpriteRenderer>(out SpriteRenderer sprite);
			sprite.color = Random.ColorHSV();

			ball.TryGetComponent(out PopSystem popSystem);
			popSystem.OnPop += UpdateScore;
		}

		private Vector2 GetRandomPosition() => 
			new Vector2(
				Random.Range(_rightBorderPosition.position.x, _leftBorderPosition.position.x),
				transform.position.y);

		private void UpdateScore(bool isPoppedByBorder)
		{
			if (isPoppedByBorder == false)
				_scoreController.IncreaseScore();
		}
	}
}