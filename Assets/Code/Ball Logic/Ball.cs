using UnityEngine;

namespace Code
{
	public class Ball : MonoBehaviour
	{
		[SerializeField] private PopSystem _popSystem;

		private void OnMouseDown()
		{
			_popSystem.PopBall();
		}
	}
}