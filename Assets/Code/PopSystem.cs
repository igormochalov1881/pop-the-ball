using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Code
{
	public class PopSystem : MonoBehaviour, IPointerDownHandler
	{
		[SerializeField] private ParticleSystem _popEffect;

		public event Action<bool> OnPop;

		private bool isBallPoppedByBorder;

        public void OnPointerDown(PointerEventData eventData) => 
			PopBall();
        
		public void PopBall(bool isPoppedByBorder = false)
		{
			isBallPoppedByBorder = isPoppedByBorder;

			AnnounceOnPop();

			GameObject particle = PlayPopEffect();
			Destroy(particle, 0.5f);

			Destroy(gameObject);
		}

        private GameObject PlayPopEffect() => 
			Instantiate(_popEffect.gameObject, transform.position, Quaternion.identity);

		private void AnnounceOnPop()
        {
			if (OnPop != null)
				OnPop.Invoke(isBallPoppedByBorder);
		}
	}
}