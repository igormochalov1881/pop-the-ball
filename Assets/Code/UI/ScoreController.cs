using System;
using UnityEngine;

namespace Code
{
	public class ScoreController : MonoBehaviour
	{
		public event Action OnIncreaseScore;
		public int score;

		[SerializeField] private UIScoreController _uiScoreController;
		
		private void Start()
		{
			OnIncreaseScore += AddScore;
			OnIncreaseScore += UpdateUI;
		}
		
		public void IncreaseScore()
		{
			OnIncreaseScore?.Invoke();
		}
		
		private void AddScore()
		{
			score++;
		}

		private void UpdateUI()
		{
			_uiScoreController.UpdateScore(score);
		}
	}
}