using TMPro;
using UnityEngine;

namespace Code
{
	public class UIScoreController : MonoBehaviour
	{
		public TMP_Text score;
		
		public void UpdateScore(int newScore)
		{
			score.text = newScore.ToString();
		}
	}
}